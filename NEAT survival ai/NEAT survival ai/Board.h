#pragma once

#include "Creature.h"


#include <SFML\Graphics.hpp>
#include <string>
#include <random>

enum foodType
{
	MEAT,
	VEGGIES,
	ROT
};

struct Tile
{
	//float height;
	float foodCount;
	foodType type;

};

class Creature;

class Board
{
public:
	Board(std::string& filepath, int numberOfCreatures, int amountOfBufferCreatures, sf::RenderWindow& window);
	~Board();
	
	float removeFood(sf::Vector2f pos, int amount);
	float getFoodAtPos(sf::Vector2f pos);

	int getAmountOfCreaturesInArea(sf::Vector2f pos, int radius);

	void update(float deltaTime, sf::RenderWindow& window);
	void display(sf::RenderWindow& window);

	bool spawnNewCreature(Creature* parent);
	
	void drawFirstCreature(sf::RenderWindow& window);

	sf::Vector2f tileSize;
private:
	std::vector<std::vector<Tile>> map;

	std::mt19937 mt;
	std::uniform_int_distribution<int> foodDist;
	std::uniform_real_distribution<float> floatDist;

	std::vector<Creature*> creatures;

	float elapsedTime = 0.0f;
	int maxFoodCount = 500;
	int minCreatures;
	int bufferCreatures;

};

