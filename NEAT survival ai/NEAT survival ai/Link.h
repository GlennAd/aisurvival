#pragma once


#include "Node.h"
#include <SFML\Graphics.hpp>

class Node;

class Link
{

public:
	Link(Link& link);
	Link(Node* inNode, Node* outNode);
	Link(Node* inNode, Node* outNode, float weight);

	void mutate();

	void draw(sf::RenderWindow& window);

	Node* inNode;
	Node* outNode;

	float value;
	float weight;

private:

	sf::RectangleShape line;

};

