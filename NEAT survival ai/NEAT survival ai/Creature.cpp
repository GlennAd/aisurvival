#include "Creature.h"
#include "Config.h"

#include <iostream>

int Creature::lastCreatureID = 0;

Creature::Creature(sf::Vector2f& pos, sf::Vector2f& maxPositions, Board* board)
{
	this->id = ++lastCreatureID;

	this->board = board;

	brain = new Genome(3, 3, 5, 4);

	for (int i = 0; i < 300; ++i)
	{
		brain->mutate();
	}

	brain->mutate();

	maxPos = maxPositions;

	rotatingSpeed = 100.0f;
	speed = 200.0f;

	currentEnergy = 5000.0f;
	maxEnergy = 10000.0f;

	eatingRate = 20.0f;

	maxSize = 20.0f;

	leftEyeRotation = -0.5f;
	rightEyeRotation = 0.5f;

	shape = sf::CircleShape(10.0f);
	shape.setFillColor(sf::Color(255, 0, 0, 255));
	shape.setOrigin(shape.getRadius(), shape.getRadius());
	shape.setPosition(pos);

	leftEye = sf::CircleShape(5.0f);
	rightEye = sf::CircleShape(5.0f);
	
	leftEye.setFillColor(sf::Color(255, 255, 0, 255));
	leftEye.setOrigin(leftEye.getRadius(), leftEye.getRadius());

	rightEye.setFillColor(sf::Color(0, 255, 255, 255));
	rightEye.setOrigin(rightEye.getRadius(), rightEye.getRadius());
}

Creature::Creature(Creature* creature)
{
	this->id = ++lastCreatureID;
	maxPos = creature->maxPos;

	brain = new Genome(creature->brain);
	this->board = creature->board;

	//for (int i = 0; i < 10; ++i)
	//{
		brain->mutate();
	//}

	//float testValue = (0.9 + (percentageDistributor(randomGen) + percentageDistributor(randomGen)) / 800.0);

	rotatingSpeed = creature->rotatingSpeed *(0.9 + (percentageDistributor(randomGen) + percentageDistributor(randomGen)) / 800.0);
	speed = creature->speed * (0.9f + (percentageDistributor(randomGen) + percentageDistributor(randomGen)) / 800.0);

	currentEnergy = creature->maxEnergy * energyCostForBreeding;
	maxEnergy = creature->maxEnergy * (0.9 + (percentageDistributor(randomGen) + percentageDistributor(randomGen)) / 800.0);

	eatingRate = creature->eatingRate * (0.9 + (percentageDistributor(randomGen) + percentageDistributor(randomGen)) / 800.0);

	maxSize = creature->maxSize * (0.9 + (percentageDistributor(randomGen) + percentageDistributor(randomGen)) / 800.0);

	shape = creature->shape;
	shape.setRadius(10.0f);
	
	leftEye = creature->leftEye;
	rightEye = creature->rightEye;

	leftEyeRotation = creature->leftEyeRotation;
	rightEyeRotation = creature->rightEyeRotation;
}


Creature::~Creature()
{
	delete brain;
}

void Creature::draw(sf::RenderWindow& window)
{
	shape.setFillColor(sf::Color((255 * currentEnergy) / maxEnergy, 0, 0, 255));
	
	window.draw(shape);
	window.draw(leftEye);
	window.draw(rightEye);
}

void Creature::drawGenome(sf::RenderWindow & window)
{
	brain->draw(window);
}


bool Creature::update(float deltaTime)
{
	//brain.mutate();
	age += deltaTime;

	if (age < 100.0f)
	{
		currentEnergy -= shape.getRadius() * 4.0f * deltaTime;
		//printf("Energy lost due to existing for creature %i: %f\n", id, shape.getRadius() * 2.0f * deltaTime);
	}
	else
	{
		currentEnergy -= shape.getRadius() * ((age - 100.0f) / 10.0f) * deltaTime * 2.0f;
		//printf("Energy lost due to existing for creature %i: %f\n", id, shape.getRadius() * ((age - 100.0f) / 10.0f) * deltaTime * 2.0f);

	}

	int creaturesCloseBy = board->getAmountOfCreaturesInArea(shape.getPosition(), shape.getRadius() * 30);

	if (creaturesCloseBy == 0)
	{
		creaturesCloseBy = 1;
	}

	float inputs[5];
	inputs[0] = board->getFoodAtPos(shape.getPosition());
	inputs[1] = currentEnergy / maxEnergy;
	inputs[2] = see(true);
	inputs[3] = see(false);
	inputs[4] = 1.0f - (1.0f / creaturesCloseBy);
	//inputs[4] = board->getAmountOfCreaturesInArea(shape.getPosition(), shape.getRadius() * 10);
	//printf("Apperent amount of creatures nearby. %i\n\n", board->getAmountOfCreaturesInArea(shape.getPosition(), shape.getRadius() * 10));


	float outputs[4];

	brain->input(inputs, outputs);

	////printf("Creature: %i\nInput 0: %f\nInput 1: %f\nValue 0: %f\nValue 1: %f\nValue 2: %f\n\n", id, inputs[0], inputs[1], outputs[0], outputs[1], outputs[2]);

	eat((outputs[0] + 1.0f) / 2.0f, deltaTime);
	breed(outputs[1]);
	rotate(outputs[2], deltaTime);
	move(outputs[3], deltaTime);
	if (currentEnergy < shape.getRadius() * 10.0f)
	{
		return false;
	}
	return true;
}

void Creature::eat(float modifier, float deltaTime)
{
	float canEat = eatingRate * shape.getRadius() * deltaTime * modifier;
	if (canEat < 0.0f)
	{
		canEat = 0.0f;
	}
	int energyDiff = 0;
	if (shape.getRadius() < maxSize)
	{
		shape.setRadius(shape.getRadius() + shape.getRadius() * 0.1f * deltaTime);
		if (shape.getRadius() > maxSize)
		{
			shape.setRadius(maxSize);
		}

		currentEnergy -= 0.5f * shape.getRadius() * modifier * deltaTime;
		//printf("Energy lost due to growth for creature %i: %f\n", id, 0.5f * shape.getRadius() * modifier * deltaTime);
		shape.setOrigin(shape.getRadius(), shape.getRadius());
	}

	float foodEaten = board->removeFood(shape.getPosition(), canEat);

	//printf("Energy gained for creature %i: %f\n", id, foodEaten);

	currentEnergy += foodEaten;

	if (currentEnergy > maxEnergy)
	{
		currentEnergy = maxEnergy;
	}
}

sf::Vector2f Creature::getPos()
{
	return shape.getPosition();
}

void Creature::rotate(float direction, float deltaTime)
{
	shape.rotate(rotatingSpeed * direction * deltaTime);
	currentEnergy -= abs(rotatingSpeed * direction * deltaTime);

	//printf("Energy lost due to rotation for creature %i: %f\n", id, abs(rotatingSpeed * direction * deltaTime));
}

void Creature::move(float direction, float deltaTime)
{
	float asRadians = shape.getRotation() * 3.141592 / 180;

	shape.move(cos(asRadians) * speed * direction * deltaTime, sin(asRadians) * speed * direction * deltaTime);
	
	currentEnergy -= abs(speed * direction * deltaTime);

	//printf("Energy lost due to moving for creature %i: %f\n\n", id, abs(speed * direction * deltaTime));

	float radius = shape.getRadius();

	if (shape.getPosition().x <= 0)
	{
		shape.setPosition(maxPos.x - 1, shape.getPosition().y);
	}
	else if (shape.getPosition().x >= maxPos.x)
	{
		shape.setPosition(1, shape.getPosition().y);
	}

	if (shape.getPosition().y <= 0)
	{
		shape.setPosition(shape.getPosition().x, maxPos.y - 1);
	}
	else if (shape.getPosition().y >= maxPos.y)
	{
		shape.setPosition(shape.getPosition().x, 1);
	}

	sf::Vector2f leftEyePos = shape.getPosition() + sf::Vector2f(cos(asRadians + leftEyeRotation) * board->tileSize.x, sin(asRadians + leftEyeRotation) * board->tileSize.y);
	sf::Vector2f rightEyePos = shape.getPosition() + sf::Vector2f(cos(asRadians + rightEyeRotation) * board->tileSize.x, sin(asRadians + rightEyeRotation) * board->tileSize.y);

	if (leftEyePos.x <= 0)
	{
		leftEyePos.x = maxPos.x + leftEyePos.x;
	}
	else if (leftEyePos.x >= maxPos.x)
	{
		leftEyePos.x = maxPos.x - leftEyePos.x;
	}

	if (leftEyePos.y <= 0)
	{
		leftEyePos.y = maxPos.y + leftEyePos.y;
	}
	else if (leftEyePos.y >= maxPos.y)
	{
		leftEyePos.y = maxPos.y - leftEyePos.y;
	}

	if (rightEyePos.x <= 0)
	{
		rightEyePos.x = maxPos.x + rightEyePos.x;
	}
	else if (rightEyePos.x >= maxPos.x)
	{
		rightEyePos.x = maxPos.x - rightEyePos.x;
	}

	if (rightEyePos.y <= 0)
	{
		rightEyePos.y = maxPos.y + rightEyePos.y;
	}
	else if (rightEyePos.y >= maxPos.y)
	{
		rightEyePos.y = maxPos.y - rightEyePos.y;
	}

	leftEye.setPosition(leftEyePos);
	rightEye.setPosition(rightEyePos);
}

void Creature::breed(float modifier)
{
	int kids = modifier / 0.1f;

	for (int i = 0; i < kids; ++i)
	{
		if ((currentEnergy / maxEnergy) > breedingThreshold && board->spawnNewCreature(this))
		{
			currentEnergy -= maxEnergy * energyCostForBreeding;
		}
	}

}

float Creature::see(bool left)
{
	if (left)
	{
		return board->getFoodAtPos(leftEye.getPosition());
	}
	else
	{
		return board->getFoodAtPos(rightEye.getPosition());
	}
}
