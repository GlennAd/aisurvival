#pragma once

#include <vector>

#include "Link.h"

#include <SFML\Graphics.hpp>

class Link;

class Node
{
public:
	Node(int column, sf::Vector2f& offset);
	Node(Node* node);
	~Node();

	void processInfo();
	float processOutputValue();

	void addInputLink(Link* link);
	void addOutputLink(Link* link);

	std::vector<Link*> deleteOutPutLinks();

	std::vector<Link*>& getInLinks();
	std::vector<Link*>& getOutLinks();

	void draw(sf::RenderWindow& window);

	int column;

	sf::CircleShape shape;

private:
	sf::Vector2f offset;

	std::vector<Link*> inLinks;
	std::vector<Link*> outLinks;
};

