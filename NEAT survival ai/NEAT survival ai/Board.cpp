#pragma warning( push )
#pragma warning( disable : 4996)

#include "Board.h"

#include <random>


Board::Board(std::string& filepath, int numberOfCreatures, int amountOfBufferCreatures, sf::RenderWindow& window)
{
	//Used for seeding mt19937
	std::random_device rd;

	mt = std::mt19937(rd());
	//mt = std::mt19937(1000);
	foodDist = std::uniform_int_distribution<int>(0, maxFoodCount);
	floatDist = std::uniform_real_distribution<float>(10.0f, window.getSize().x);

	tileSize = sf::Vector2f(window.getSize().x / 20, window.getSize().y / 20);

	minCreatures = numberOfCreatures;
	bufferCreatures = amountOfBufferCreatures;

	int x, y, temp;
	filepath = filepath;
	FILE* file = fopen(filepath.c_str(), "r");

	fscanf(file, "%dx%d", &x, &y);

	for (int i = 0; i < y; ++i)
	{
		std::vector<Tile> row;
		for (int j = 0; j < x; ++j)
		{
			Tile temp;
			temp.foodCount = foodDist(mt);
			temp.type = foodType::VEGGIES;
			row.push_back(temp);
		}
		map.push_back(row);
	}

	fclose(file);
	file = nullptr;

	for (int i = 0; i < numberOfCreatures + amountOfBufferCreatures; ++i)
	{
		Creature* creature = new Creature(sf::Vector2f(floatDist(mt), floatDist(mt)), sf::Vector2f(map.at(0).size() * tileSize.x, map.size() * tileSize.y), this);
		creatures.push_back(creature);
	}

	//printf("Testing");

	creatures.reserve(50);
}

Board::~Board()
{
	std::vector<Creature*>::iterator creatureIterator;

	for (creatureIterator = creatures.begin(); creatureIterator != creatures.end(); ++creatureIterator)
	{
		delete (*creatureIterator);
	}
}

float Board::removeFood(sf::Vector2f pos, int amount)
{
	sf::Vector2i tileCoords = sf::Vector2i(pos.x / tileSize.x, pos.y / tileSize.y);
	if (tileCoords.x < 0 || tileCoords.x > map.at(0).size() || tileCoords.y < 0 || tileCoords.y > map.size())
	{
		printf("Error, trying to eat outside the map.\nCoords: %i, %i\n\n", tileCoords.x, tileCoords.y);
	}
	else
	{
		float food = map.at(tileCoords.y).at(tileCoords.x).foodCount;
		if (food < amount)
		{
			map.at(tileCoords.y).at(tileCoords.x).foodCount = 0;
			return food;
		}
		else
		{
			map.at(tileCoords.y).at(tileCoords.x).foodCount -= amount;
			return amount;
		}
	}
}

float Board::getFoodAtPos(sf::Vector2f pos)
{
	sf::Vector2i tileCoords = sf::Vector2i(pos.x / tileSize.x, pos.y / tileSize.y);

	if (tileCoords.y >= map.size() || tileCoords.y < 0)
	{ 
		tileCoords.y = 0;
	}

	if (tileCoords.x >= map[0].size() || tileCoords.x < 0)
	{
		tileCoords.x = 0;
	}

	return map.at(tileCoords.y).at(tileCoords.x).foodCount / (float)maxFoodCount;
}

int Board::getAmountOfCreaturesInArea(sf::Vector2f pos, int radius)
{
	std::vector<Creature*>::iterator creature;
	int amount = 0;

	for (creature = creatures.begin(); creature != creatures.end(); ++creature)
	{
		sf::Vector2f diff = pos - (*creature)->getPos();

		float dist = sqrt(diff.x * diff.x + diff.y * diff.y);

		if (dist < radius)
		{
			++amount;
		}
	}

	return amount;
}

void Board::update(float deltaTime, sf::RenderWindow& window)
{
	//printf("Creatures in vector: %i\n\n\n", creatures.size());

	for (int y = 0; y < map.size(); ++y)
	{
		for (int x = 0; x < map.at(y).size(); ++x)
		{
			if (map.at(y).at(x).foodCount < maxFoodCount)
			{
				map.at(y).at(x).foodCount += 1.0f * deltaTime;
			}
		}
	}

	sf::Vector2i tileCoords;

	for (int i = 0; i < creatures.size(); ++i)
	{
		if (!creatures.at(i)->update(deltaTime))
		{
			delete creatures[i];
			creatures.erase(creatures.begin() + i);
			--i;
		}
		/*else
		{
			tileCoords = sf::Vector2i(creatures.at(i).getPos().x / tileSize.x, creatures.at(i).getPos().y / tileSize.y);

			float amount = creatures.at(i).eat(map.at(tileCoords.y).at(tileCoords.x).foodCount, deltaTime);
			
			map.at(tileCoords.y).at(tileCoords.x).foodCount -= amount;
		}*/
	}

	if (creatures.size() < minCreatures)
	{
		if (creatures.size() > 0)
		{
			std::uniform_int_distribution<int> creaturePicker(0, creatures.size() - 1);
			int choice;

			while (creatures.size() < minCreatures + bufferCreatures)
			{
				choice = creaturePicker(mt);
				//Creature* creature = new Creature(creatures.at(choice));
				Creature* creature = new Creature(sf::Vector2f(floatDist(mt), floatDist(mt)), sf::Vector2f(map.at(0).size() * tileSize.x, map.size() * tileSize.y), this);
				creatures.push_back(creature);

				printf("Creature spawned by god.\n\n");
			}
		}
		else
		{
			Creature* creature = new Creature(sf::Vector2f(floatDist(mt), floatDist(mt)), sf::Vector2f(map.at(0).size() * tileSize.x, map.size() * tileSize.y), this);
			creatures.push_back(creature);
		}
	}
}


void Board::display(sf::RenderWindow & window)
{
	sf::RectangleShape tile;
	tile.setSize(tileSize);

	for (int y = 0; y < map.size(); ++y)
	{
		for (int x = 0; x < map.at(y).size(); ++x)
		{
			tile.setFillColor(sf::Color(0, ((float)(map.at(y).at(x).type * map.at(y).at(x).foodCount) / maxFoodCount) * 255, 0, 255));
			tile.setPosition((float)x * tile.getSize().x, (float)y * tile.getSize().y);
			window.draw(tile);
		}
	}

	for (int i = 0; i < creatures.size(); ++i)
	{
		creatures.at(i)->draw(window);
	}
}

bool Board::spawnNewCreature(Creature* parent)
{
	if (creatures.size() < 200)
	{
		Creature* creature = new Creature(parent);
		creatures.push_back(creature);
		printf("Creature spawned due to breeding.\nAmount of creatures alive: %i\n\n", creatures.size());
		return true;
	}
	
	return false;
}

void Board::drawFirstCreature(sf::RenderWindow & window)
{
	if (!creatures.empty())
	{
		creatures.at(0)->drawGenome(window);
	}
}

