#pragma once

#include "Genome.h"
#include "Board.h"

#include <SFML\Graphics.hpp>

class Board;

class Creature
{
public:
	Creature(sf::Vector2f& pos, sf::Vector2f& maxPositions, Board* board);
	
	Creature(Creature* creature);
	~Creature();


	void draw(sf::RenderWindow& window);
	void drawGenome(sf::RenderWindow& window);

	bool update(float deltaTime);

	sf::Vector2f getPos();

	static int lastCreatureID;
private:
	void eat(float modifier, float deltaTime);
	void rotate(float direction, float deltaTime);
	void move(float direction, float deltaTime);
	void breed(float modifier);
	float see(bool left);

	Genome* brain;// = Genome(3, 3, 3, 3);

	sf::CircleShape shape;
	sf::Vector2f maxPos;
	sf::CircleShape leftEye;
	sf::CircleShape rightEye;

	Board* board;
	
	float rotatingSpeed;
	float speed;

	float currentEnergy;
	float maxEnergy;

	float eatingRate;

	float maxSize;

	float age = 0.0f;

	unsigned int id;

	float leftEyeRotation;
	float rightEyeRotation;
};

