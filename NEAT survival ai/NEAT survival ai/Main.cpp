
#include "Board.h"
#include "Creature.h"
#include "Link.h"
#include "Gene.h"
#include "Genome.h"

#include <iostream>
#include <SFML\Graphics.hpp>

void main()
{
	sf::RenderWindow window(sf::VideoMode(1000.f, 1000.f), "NEAT survival ai", sf::Style::Close);

	std::string filepath = "./resources/config";
	//Board board(filepath, 20, 5, window);

	sf::View mainView(sf::FloatRect(0, 0, window.getSize().x * 4.0f, window.getSize().y * 4.0f));
	mainView.setViewport(sf::FloatRect(0.0f, 0.f, 0.5f, 0.5f));

	sf::View genomeView(sf::FloatRect(0, 0, window.getSize().x * 4.0f, window.getSize().y * 2.f));
	genomeView.setViewport(sf::FloatRect(0.0f, 0.5f, 1.0f, 0.5f));
	
	sf::Clock clock;
	float timeModifier = 1.0f;

	bool up = false, down = false;

	//Gene genetest(sf::Vector2f(100.0f, 0.0f));
	
	//Genome genometest(2, 2, 1, 2);
	//for (int i = 0; i < 10; ++i)
	//{
	//	genometest.mutate();
	//}
	//genometest.writeToFile(0);
	

	Genome genometest2(0);

	//float testInputValues[3];
	//testInputValues[0] = 1.0f;
	//testInputValues[1] = 1.0f;
	//testInputValues[2] = 1.0f;
	//
	//float testOutputValues[3];

	while (window.isOpen())
	{
		sf::Event e;

		while (window.pollEvent(e))
		{
			if (e.type == sf::Event::Closed)
			{
				window.close();
			}
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && window.hasFocus())
		{
			system("Pause");
			clock.restart();
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && !up && window.hasFocus())
		{
			timeModifier *= 2.0f;
			if (timeModifier == 0.0f)
			{
				timeModifier = 1.0f;
			}
			printf("Time Modifier is: %f\n\n", timeModifier);

			up = true;
		}
		else if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			up = false;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) && !down && window.hasFocus())
		{
			timeModifier /= 2.0f;

			printf("Time Modifier is: %f\n\n", timeModifier);
		}
		else if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		{
			down = false;
		}

		float time = clock.restart().asSeconds();
		if (time > 0.05f)
		{
			time = 0.05f;
		}
		float deltaTime = time * timeModifier;

		window.clear(sf::Color(255, 0, 0, 255));
		window.setView(mainView);

		//board.update(deltaTime, window);
		//board.display(window);
		
		window.setView(genomeView);
		//board.drawFirstCreature(window);

		genometest2.draw(window);

		//genometest.mutate();
		//genometest.input(testInputValues, testOutputValues);
		////printf("Value 0: %f\nValue 1: %f\n\n\n\n", testOutputValues[0], testOutputValues[1]);
		//genometest.draw(window);
		

		window.display();
	}
	
	

	//printf("Testing");
}